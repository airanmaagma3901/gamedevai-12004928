using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleMovement : MonoBehaviour
{
    public Transform goal;
    [SerializeField] float speed;
    [SerializeField] float rotSpeed;
    [SerializeField] float acceleration;
    [SerializeField] float deceleration;
    [SerializeField] float minSpeed;
    [SerializeField] float maxSpeed;
    [SerializeField] float brakeAngle;

    void LateUpdate()
    {
        Vector3 LookAtGoal = new Vector3(goal.transform.position.x, 
                                        this.transform.position.y, 
                                        goal.transform.position.z);
        
        Vector3 Direction = LookAtGoal - transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                    Quaternion.LookRotation(Direction),
                                                    Time.deltaTime * rotSpeed);

        if(Vector3.Angle(goal.forward, this.transform.forward) > brakeAngle && speed > 5)
        {
            speed = Mathf.Clamp(speed - (deceleration * Time.deltaTime), minSpeed , maxSpeed);
        }

        else
        {
            speed = Mathf.Clamp(speed + (acceleration * Time.deltaTime), minSpeed , maxSpeed);
        }

        this.transform.Translate(0, 0, speed);
    }
}