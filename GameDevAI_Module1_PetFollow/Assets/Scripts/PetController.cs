using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetController : MonoBehaviour
{
    public Transform player;
    [SerializeField] float rotSpeed = 2;
    [SerializeField] float moveSpeed = 1;
    void LateUpdate()
    {
        Vector3 lookAtGoal = new Vector3(player.position.x,
                                        this.transform.position.y,
                                        player.position.z);

        Vector3 direction = lookAtGoal - transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                    Quaternion.LookRotation(direction),
                                                    Time.deltaTime * rotSpeed);

        if (Vector3.Distance(lookAtGoal, transform.position) > 2)
        {
            transform.position = Vector3.Lerp(this.transform.position, player.transform.position, moveSpeed * Time.deltaTime);
        }
    }
}