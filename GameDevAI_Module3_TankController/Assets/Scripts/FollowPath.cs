using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class FollowPath : MonoBehaviour
{
    Transform goal;
    float speed = 10.0f;
    float accuracy = 1.0f;
    float rotSpeed = 15.0f;
    public GameObject wpManager;
    GameObject[] wps;
    GameObject currentNode;
    int currentWaypointIndex = 0;
    Graph graph;
    public GameObject[] Tanks;
    int RedCurrentNode, BlueCurrentNode, GreenCurrentNode;
    int controlledTank = 0;

    void Start()
    {
        wps = wpManager.GetComponent<WaypointManager>().waypoints;
        graph = wpManager.GetComponent<WaypointManager>().graph;
        currentNode = wps[0];
    }

    void LateUpdate()
    {
        
        if (graph.getPathLength() == 0 || currentWaypointIndex == graph.getPathLength()) 
        {
            return;
        }

        currentNode = graph.getPathPoint(currentWaypointIndex);

        //Waypoint Switch
        if (Vector3.Distance(graph.getPathPoint(currentWaypointIndex).transform.position,
                            Tanks[controlledTank].transform.position) < accuracy)
        {
            currentWaypointIndex++;
        }

        //Tank Moving
        if (currentWaypointIndex < graph.getPathLength())
        {
            goal = graph.getPathPoint(currentWaypointIndex).transform;

            Vector3 lookAtGoal = new Vector3(goal.position.x,
                                            Tanks[controlledTank].transform.position.y,
                                            goal.position.z);

            Vector3 direction = lookAtGoal - Tanks[controlledTank].transform.position;

            Tanks[controlledTank].transform.rotation = Quaternion.Slerp(Tanks[controlledTank].transform.rotation,
                                                        Quaternion.LookRotation(direction),
                                                        Time.deltaTime * rotSpeed);
            Tanks[controlledTank].transform.Translate(0, 0, speed * Time.deltaTime);
        }
    }

    public void ControlRed()
    {
        controlledTank = 1;
        currentNode = wps[RedCurrentNode];
    }
    public void ControlBlue()
    {
        controlledTank = 2;
        currentNode = wps[BlueCurrentNode];
    }
    public void ControlGreen()
    {
        controlledTank = 0;
        currentNode = wps[GreenCurrentNode];
    }
    public void GoToHelipad()
    {
        switch(controlledTank)
        {
            case 0: 
            GreenCurrentNode = 5;
            break;

            case 1:
            RedCurrentNode = 5;
            break;

            case 2:
            BlueCurrentNode = 5;
            break;
        }
        graph.AStar(currentNode, wps[5]);
        currentWaypointIndex = 0;
    }
    public void GoToRuins()
    {
        switch(controlledTank)
        {
            case 0: 
            GreenCurrentNode = 14;
            break;

            case 1:
            RedCurrentNode = 14;
            break;

            case 2:
            BlueCurrentNode = 14;
            break;
        }
        graph.AStar(currentNode, wps[14]);
        currentWaypointIndex = 0;
    }
    public void GoToFactory()
    {
        switch(controlledTank)
        {
            case 0: 
            GreenCurrentNode = 17;
            break;

            case 1:
            RedCurrentNode = 17;
            break;

            case 2:
            BlueCurrentNode = 17;
            break;
        }
        graph.AStar(currentNode, wps[17]);
        currentWaypointIndex = 0;
    }
    public void GoToTwinMountains()
    {
        switch(controlledTank)
        {
            case 0: 
            GreenCurrentNode = 19;
            break;

            case 1:
            RedCurrentNode = 19;
            break;

            case 2:
            BlueCurrentNode = 19;
            break;
        }
        graph.AStar(currentNode, wps[19]);
        currentWaypointIndex = 0;
    }
    public void GoToBarracks()
    {
        switch(controlledTank)
        {
            case 0: 
            GreenCurrentNode = 18;
            break;

            case 1:
            RedCurrentNode = 18;
            break;

            case 2:
            BlueCurrentNode = 18;
            break;
        }
        graph.AStar(currentNode, wps[18]);
        currentWaypointIndex = 0;
    }
    public void GoToCommandCenter()
    {
        switch(controlledTank)
        {
            case 0: 
            GreenCurrentNode = 1;
            break;

            case 1:
            RedCurrentNode = 1;
            break;

            case 2:
            BlueCurrentNode = 1;
            break;
        }
        graph.AStar(currentNode, wps[1]);
        currentWaypointIndex = 0;
    }
    public void GoToOilRefineryPumps()
    {
        switch(controlledTank)
        {
            case 0: 
            GreenCurrentNode = 16;
            break;

            case 1:
            RedCurrentNode = 16;
            break;

            case 2:
            BlueCurrentNode = 16;
            break;
        }
        graph.AStar(currentNode, wps[16]);
        currentWaypointIndex = 0;
    }
    public void GoToTankers ()
    {
        switch(controlledTank)
        {
            case 0: 
            GreenCurrentNode = 11;
            break;

            case 1:
            RedCurrentNode = 11;
            break;

            case 2:
            BlueCurrentNode = 11;
            break;
        }
        graph.AStar(currentNode, wps[11]);
        currentWaypointIndex = 0;
    }
    public void GoToRadar ()
    {
        switch(controlledTank)
        {
            case 0: 
            GreenCurrentNode = 8;
            break;

            case 1:
            RedCurrentNode = 8;
            break;

            case 2:
            BlueCurrentNode = 8;
            break;
        }
        graph.AStar(currentNode, wps[8]);
        currentWaypointIndex = 0;
    }
    public void GoToCommandPost ()
    {
        switch(controlledTank)
        {
            case 0: 
            GreenCurrentNode = 15;
            break;

            case 1:
            RedCurrentNode = 15;
            break;

            case 2:
            BlueCurrentNode = 15;
            break;
        }
        graph.AStar(currentNode, wps[15]);
        currentWaypointIndex = 0;   
    }
    public void GoToMiddleMap ()
    {
        switch(controlledTank)
        {
            case 0: 
            GreenCurrentNode = 10;
            break;

            case 1:
            RedCurrentNode = 10;
            break;

            case 2:
            BlueCurrentNode = 10;
            break;
        }
        graph.AStar(currentNode, wps[10]);
        currentWaypointIndex = 0; 
    }
}