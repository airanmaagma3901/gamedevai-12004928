using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Gold : MonoBehaviour
{
    public static int goldAmount = 5;

    public static void AddGold(int reward)
    {
        goldAmount += reward;
    }

    public static void DeductGold(int price)
    {
        goldAmount -= price;
    }
}