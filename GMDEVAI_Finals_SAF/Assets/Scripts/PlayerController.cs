using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Vector3 movement;
    Animator anim;
    
    public GameObject bullet;
    public GameObject weapon;
    public HealthBar healthBar;

    [SerializeField] AudioClip shootingClip;
    [SerializeField] AudioSource runningClip;
    [SerializeField] AudioClip purchaseClip;
    [SerializeField] AudioClip reloadClip;
    
    //Movement Stats
    public float moveSpeed = 20;
    public float rotateSpeed = 20;

    //Player Stats
    public static float currentHealth;
    float maxHealth = 10f;
    public bool isAlive = true;
    public bool isWinner = false;

    //Weapon Stats
    public int maxAmmoCount = 30;
    public int currentAmmoCount;
    int reloadTime = 1;
    float attackSpeed = .2f;
    public bool isReloading = false;
    bool canFire = true;
    
    void Start()
    {
        anim = this.GetComponent<Animator>();
        Gold.goldAmount = 5;

        //Weapon
        currentAmmoCount = maxAmmoCount;

        //Player 
        isAlive = true;
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
    }

    void Update()
    {
        if (!isAlive) return;

        //Movement input
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.z = Input.GetAxisRaw("Vertical");
        movement.Normalize();

        //Health tracker
        if(currentHealth <= 0)  
        {
            isAlive = false;
        }

        //Weapon shooting and reloading
        if(isReloading) return;

        if (Input.GetKeyDown(KeyCode.R) || currentAmmoCount <= 0) 
        {
            StartCoroutine(Reload(reloadTime));
        }

        if(Input.GetKey(KeyCode.Space) && canFire)
        {
            anim.SetBool("IsShooting", true);
            currentAmmoCount--;
            StartCoroutine(Fire(attackSpeed));
        }
        else anim.SetBool("IsShooting", false);

        if(Input.GetKeyDown(KeyCode.H) && Gold.goldAmount >= 5 && currentHealth < maxHealth) heal();
    }

    IEnumerator Reload(int reloadTime)
    {
        isReloading = true;
        AudioSource.PlayClipAtPoint(reloadClip, Camera.main.transform.position, .2f);

        yield return new WaitForSeconds(reloadTime);

        currentAmmoCount = maxAmmoCount;
        isReloading = false;
    }

    IEnumerator Fire(float attackSpeed)
    {
        AudioSource.PlayClipAtPoint(shootingClip, Camera.main.transform.position, .05f);
        GameObject _bullet = Instantiate(bullet, weapon.transform.position, weapon.transform.rotation);
        _bullet.GetComponent<Rigidbody>().AddForce(weapon.transform.forward * 5000);
        Destroy(_bullet, 2.0f);
        canFire = false;

        yield return new WaitForSeconds(attackSpeed);

        canFire = true;
    }   

    public void TakeDamage(float damage)
    {
        currentHealth -= damage;
        healthBar.SetHealth(currentHealth);
    }

    void LateUpdate()
    {
        //Camera placement on top of player (topdown effect)
        Camera.main.transform.position = new Vector3(this.transform.position.x,
                                                this.transform.position.y + 50,
                                                this.transform.position.z);

        //Player movement
        transform.Translate(movement * moveSpeed * Time.deltaTime, Space.World);

        //Player rotation always in direction of movement
        if(movement != Vector3.zero) 
        {   
            runningClip.enabled = true;
            anim.SetBool("IsMoving", true);
            transform.forward = movement;
        }
        else 
        {
            runningClip.enabled = false;
            anim.SetBool("IsMoving", false);
        }
    }

    void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "Turret")
        {
            if (Input.GetKeyDown(KeyCode.F) && Gold.goldAmount >= 25)
            {
                AudioSource.PlayClipAtPoint(purchaseClip, Camera.main.transform.position, .1f);
                col.gameObject.GetComponent<TurretController>().SetActive();
                Gold.DeductGold(25);
            }
        }
    }

    void heal()
    {
        currentHealth++;
        healthBar.SetHealth(currentHealth);
        Gold.DeductGold(5);
    }
}