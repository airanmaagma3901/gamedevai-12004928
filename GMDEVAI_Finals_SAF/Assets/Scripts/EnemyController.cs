using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    Animator anim;
    public UnityEngine.AI.NavMeshAgent agent;
    private PlayerController player;
    GameObject target;
    PlayerController movementController;
    [SerializeField] AudioClip deathSound;

    //Enemy Stats
    float health;
    float attackSpeed;
    float attackDamage;
    bool canAttack = true;

    void Start()
    {
        anim = this.GetComponent<Animator>();
        health = Random.Range(2,5);
        attackSpeed = Random.Range(.5f, 1.0f);
        attackDamage = Random.Range(.1f, .5f);

        player = FindFirstObjectByType<PlayerController>();
        target = GameObject.FindGameObjectWithTag("Player");
        movementController = target.GetComponent<PlayerController>();
        this.agent = this.GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    void Update()
    {
        Pursue();

        if(health <= 0)
        {
            Gold.AddGold(1);
            EnemySpawner.spawnedEnemies--;
            AudioSource.PlayClipAtPoint(deathSound, transform.position);
            Destroy(this.gameObject);
        }
    }

    void Seek(Vector3 location)
    {
        agent.SetDestination(target.transform.position);
    }

    void Pursue()
    {
        Vector3 targetDirection = target.transform.position - this.transform.position;
        float lookAhead = targetDirection.magnitude/(agent.speed + movementController.moveSpeed);
        Seek(target.transform.position + target.transform.forward * lookAhead);
    }

    void OnTriggerStay(Collider col)
    {
        if(col.gameObject.tag == "Player" && canAttack)
        {
            StartCoroutine(Attack(attackSpeed));
        }
    }

    IEnumerator Attack(float attackSpeed)
    {
        player.TakeDamage(attackDamage);
        canAttack = false;
        anim.SetBool("CanAttack", true);
        
        yield return new WaitForSeconds(attackSpeed);
    
        anim.SetBool("CanAttack", false);
        canAttack = true;
    }

    public void TakeDamage()
    {
        health--;
    }
}