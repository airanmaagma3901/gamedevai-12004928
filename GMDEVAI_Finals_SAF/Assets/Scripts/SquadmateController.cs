using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquadmateController : MonoBehaviour
{
    public UnityEngine.AI.NavMeshAgent agent;
    Animator anim;
    GameObject[] target;
    GameObject[] wps;
    PlayerController player;
    [SerializeField] GameObject bullet;
    [SerializeField] GameObject weapon;
    [SerializeField] GameObject wpManager;
    [SerializeField] UIManager uiManager;

    [SerializeField] AudioClip shootingClip;
    [SerializeField] AudioSource runningClip;

    //Character stats
    float attackSpeed = 1.0f;
    float rotationSpeed = 100.0f;
    bool canFire = true;
    bool followingPlayer = true;

    void Start()
    {
        anim = GetComponent<Animator>();
        agent = this.GetComponent<UnityEngine.AI.NavMeshAgent>();
        player = FindFirstObjectByType<PlayerController>();
        wps = wpManager.GetComponent<WaypointManager>().waypoints;
    }

    void Update()
    {   
        if(followingPlayer) agent.SetDestination(player.gameObject.transform.position);

        if(this.agent.remainingDistance > this.agent.stoppingDistance) anim.SetBool("IsMoving", true);
        else anim.SetBool("IsMoving", false);

        LookAtTarget();
    }

    void OnTriggerStay(Collider col)
    {
        if(col.gameObject.tag == "Zombie" && canFire)
        {
            StartCoroutine(Fire(attackSpeed));
        }
    }

    IEnumerator Fire(float attackSpeed)
    {
        AudioSource.PlayClipAtPoint(shootingClip, Camera.main.transform.position, .05f);
        GameObject _bullet = Instantiate(bullet, weapon.transform.position, weapon.transform.rotation);
        _bullet.GetComponent<Rigidbody>().AddForce(weapon.transform.forward * 5000);
        Destroy(_bullet, 2.0f);

        canFire = false;

        yield return new WaitForSeconds(attackSpeed);

        canFire = true;
    }

    void LookAtTarget()
    {
        target = GameObject.FindGameObjectsWithTag("Zombie");
        
        if(target.Length == 0) return;

        Vector3 lookAtTarget = new Vector3(target[0].transform.position.x,
                                            this.transform.position.y,
                                            target[0].transform.position.z);

        Vector3 direction = lookAtTarget - transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                Quaternion.LookRotation(direction),
                                                Time.deltaTime* rotationSpeed);
    }
    public void OnSquadmateClicked()
    {
        uiManager.OpenPanel();
    }

    public void GoToMainArea()
    {
        followingPlayer = false;
        agent.SetDestination(wps[0].transform.position);
        uiManager.ClosePanel();
    }

    public void GoToStorageRoom()
    {
        followingPlayer = false;
        agent.SetDestination(wps[3].transform.position);
        uiManager.ClosePanel();
    }

    public void GoToLobbyRoom()
    {
        followingPlayer = false;
        agent.SetDestination(wps[2].transform.position);
        uiManager.ClosePanel();
    }
    public void GoToKitchenRoom()
    {
        followingPlayer = false;
        agent.SetDestination(wps[1].transform.position);
        uiManager.ClosePanel();
    }
    public void GoToDiningRoom()
    {
        followingPlayer = false;
        agent.SetDestination(wps[4].transform.position);
        uiManager.ClosePanel();
    }

    public void FollowPlayer()
    {
        followingPlayer = true;
        uiManager.ClosePanel();
    }
}