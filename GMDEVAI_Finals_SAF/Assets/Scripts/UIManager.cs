using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    [SerializeField] Text GoldCounter;
    [SerializeField] Text WaveNumber;
    [SerializeField] Text EnemiesLeft;
    [SerializeField] GameObject LocationsPanel;
    [SerializeField] Text AmmoCount;
    [SerializeField] EnemySpawner enemySpawner;
    PlayerController player;

    void Start()
    {
        player = GameObject.FindFirstObjectByType<PlayerController>();
    }
    void Update()
    {
        GoldCounter.text = "Gold: " + Gold.goldAmount;
        EnemiesLeft.text = "Enemies Left: " + EnemySpawner.spawnedEnemies;

        if (enemySpawner.waveNumber == enemySpawner.totalWaveNumber) WaveNumber.text = "FINAL WAVE";
        else WaveNumber.text = "Wave: " + enemySpawner.waveNumber;
        
        if(player.isReloading) AmmoCount.text = "Reloading...";
        else AmmoCount.text = "Ammo: " + player.currentAmmoCount + " / " + player.maxAmmoCount;
    }

    public void ClosePanel()
    {
        LocationsPanel.SetActive(false);
    }

    public void OpenPanel()
    {
        LocationsPanel.SetActive(true);
    }
}