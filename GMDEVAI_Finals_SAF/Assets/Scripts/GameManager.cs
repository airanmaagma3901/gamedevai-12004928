using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    PlayerController player;
    [SerializeField] GameObject EndGamePanel;
    [SerializeField] GameObject LostGamePanel;
    
    void Start()
    {
        Time.timeScale = 1;
        player = GameObject.FindFirstObjectByType<PlayerController>();
    }

    void Update()
    {
        if (player.isWinner)
        {
            Time.timeScale = 0;
            EndGamePanel.SetActive(true);
        }

        if (!player.isAlive)
        {
            Time.timeScale = 0;
            LostGamePanel.SetActive(true);
        }
    }
}