using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretController : MonoBehaviour
{
    GameObject[] target;
    [SerializeField] GameObject bullet;
    [SerializeField] GameObject nozzle;
    [SerializeField] AudioClip shootingClip;

    //Turret Stats
    float attackSpeed = .2f;
    float rotationSpeed = 100.0f;
    bool canFire = true;
    bool isAlive = false;
    
    void Update()
    {
        if (!isAlive) return;
        LookAtTarget();
    }

    void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "Zombie" && canFire && isAlive)
        {
            StartCoroutine(Fire(attackSpeed));
        }
    }

    IEnumerator Fire(float attackSpeed)
    {
        AudioSource.PlayClipAtPoint(shootingClip, Camera.main.transform.position, .1f);
        GameObject _bullet = Instantiate(bullet, nozzle.transform.position, nozzle.transform.rotation);
        _bullet.GetComponent<Rigidbody>().AddForce(nozzle.transform.forward * 5000);
        Destroy(_bullet, 2.0f);
        canFire = false;

        yield return new WaitForSeconds(attackSpeed);

        canFire = true;
    }

    void LookAtTarget()
    {
        target = GameObject.FindGameObjectsWithTag("Zombie");
        
        if(target.Length == 0) return;

        Vector3 lookAtTarget = new Vector3(target[0].transform.position.x,
                                            this.transform.position.y,
                                            target[0].transform.position.z);

        Vector3 direction = lookAtTarget - transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                Quaternion.LookRotation(direction),
                                                Time.deltaTime* rotationSpeed);
    }
    public void SetActive()
    {
        isAlive = true;
    }
}