using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Zombie")
        {
            col.gameObject.GetComponent<EnemyController>().TakeDamage();
            Destroy(this.gameObject);
        }

        if(col.gameObject.tag == "Wall")
        {
            Destroy(this.gameObject);
        }
    }
}