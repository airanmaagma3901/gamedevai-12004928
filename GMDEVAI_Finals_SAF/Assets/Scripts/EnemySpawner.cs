using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] int SpawnTimer;
    [SerializeField] GameObject ZombiePrefab;
    PlayerController player;
    
    //Wave Stats
    public int waveNumber = 1;
    public int totalWaveNumber = 3; 
    int currentEnemyCount = 0; //To keep track of how many zombies have spawned
    int numOfSpawns = 5; //Num of spawns per wave
    int zombiesAtATime = 1; //Num of zombies to be spawned per call
    float downTime = 5; //Downtime in between waves
    bool canSpawn = true;

    public static int spawnedEnemies;

    void Start()
    {
        player = GameObject.FindFirstObjectByType<PlayerController>();
    }

    void Update()
    {
        while(currentEnemyCount != numOfSpawns && canSpawn) StartCoroutine(SpawnEnemies(2));

        if (spawnedEnemies == 0) player.isWinner = true;
    }

    IEnumerator SpawnEnemies(int SpawnTimer)
    {
        currentEnemyCount++;
        canSpawn = false;
        for (int i = 0; i < zombiesAtATime; i++) 
        {
            spawnedEnemies++;
            Instantiate(ZombiePrefab, this.transform.position, this.transform.rotation);
        }
        
        yield return new WaitForSeconds(SpawnTimer);

        if (currentEnemyCount == numOfSpawns && waveNumber < totalWaveNumber) StartCoroutine(NextWave(downTime));
        canSpawn = true;
    }

    IEnumerator NextWave(float downTime)
    {   
        waveNumber++;

        yield return new WaitForSeconds(downTime);
        
        currentEnemyCount = 0;
        zombiesAtATime++;
    }
}