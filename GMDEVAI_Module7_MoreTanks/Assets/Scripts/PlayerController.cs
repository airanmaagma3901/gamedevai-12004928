using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

 	public GameObject bullet;
    public GameObject turret;
    [SerializeField] public float playerHealth = 100.0f;
    [SerializeField] float reloadTime = 1.0f;
    bool canShoot;

    void Start()
    {
        canShoot = true;
    }

    void Update()
    {
        if(Input.GetKey(KeyCode.Space) && canShoot)
        {
            StartCoroutine(Fire());
            canShoot = false;
        }

        if (playerHealth <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "EnemyBullet")
        {
            playerHealth -= 10.0f;
        }
    }

    IEnumerator Fire()
    {
        GameObject b = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
        b.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);

        yield return new WaitForSeconds(reloadTime);

        canShoot = true;
    }
}