using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankAI : MonoBehaviour
{
    Animator anim;
    public GameObject player;
    public GameObject bullet;
    public GameObject turret;
    
    public GameObject GetPlayer()
    {
        return player;
    }

    void Start()
    {
        anim = this.GetComponent<Animator>();
    }

    void Update()
    {
        if (player != null)
        {
            anim.SetFloat("distance", Vector3.Distance(transform.position, player.transform.position));
        }
        
        if (anim.GetFloat("health") <= 0)
        {
            Destroy(this.gameObject);
        }
    }
    
    void Fire()
    {
        GameObject b = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
        b.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);
    }
    
    public void StartFiring()
    {
        InvokeRepeating("Fire", 1.0f, 1.0f);
    }

    public void StopFiring()
    {
        CancelInvoke("Fire");
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            TakeDamage();
        }
    }

    public void TakeDamage()
    {
        anim.SetFloat("health", anim.GetFloat("health") - 10.0f);
    }
}
